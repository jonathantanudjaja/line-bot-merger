const express = require("express");
const line = require("@line/bot-sdk");
const axios = require("axios");
const PORT = process.env.PORT || 3000;

const { botConfig, ...lineConfig } = require("./config");
const client = new line.Client(lineConfig);

const app = express();
app.get("/", (_, res) => {
  res.send("hello world!");
});
app.post("/webhook", line.middleware(lineConfig), (req, res) => {
  Promise.all(req.body.events.map(handleEvent))
    .then(result => res.json(result))
    .catch(err => console.error(err));
});

function replyMessage(replyToken, message) {
  client.replyMessage(replyToken, message);
}

function getJsonFromUrl(query) {
  let result = {};
  query.split("&").forEach(function(part) {
    const item = part.split("=");
    result[item[0]] = decodeURIComponent(item[1]);
  });
  return result;
}

async function handleEvent(event) {
  const profile = await client.getProfile(event.source.userId);
  const eventData = { profile, ...event };
  switch (eventData.type) {
    case "message":
      return messageHandler(eventData);
    case "postback":
      return postbackHandler(eventData);
    case "join":
      return joinHandler(eventData);
    default:
      return Promise.resolve(null);
  }
}

async function messageHandler(event) {
  switch (event.message.type) {
    case "text":
      return await textMessageHandler(event);
    default:
      return Promise.resolve(null);
  }
}

async function textMessageHandler(event) {
  const messageLowerCase = event.message.text.toLowerCase();

  let resp = null;
  for (const bot of botConfig) {
    if (messageLowerCase.indexOf(bot.name) !== -1) {
      const hookResponse = await axios.post(bot.webhook, { event });
      if (hookResponse.status === 200) {
        resp = hookResponse.data;
      }
      break;
    }
  }

  if (resp) {
    return replyMessage(event.replyToken, resp);
  } else {
    return Promise.resolve(null);
  }
}

async function postbackHandler(event) {
  const data = getJsonFromUrl(event.postback.data);

  let resp = null;
  for (const bot of botConfig) {
    if (data.botName.indexOf(bot.name) !== -1) {
      const hookResponse = await axios.post(bot.webhook, {
        event: { data, ...event }
      });
      if (hookResponse.status === 200) {
        resp = hookResponse.data;
      }
      break;
    }
  }

  if (resp) {
    return replyMessage(event.replyToken, resp);
  } else {
    return Promise.resolve(null);
  }
}

async function joinHandler(event) {
  // call all bot webhook
  for (const bot of botConfig) {
    await axios.post(bot.webhook, { event });
  }

  client.replyMessage(event.replyToken, {
    type: "text",
    text: "Hello!"
  });
}

app.listen(PORT, () => console.log(`Listening on ${PORT}`));
